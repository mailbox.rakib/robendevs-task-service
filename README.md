# robendevs-task-service


## Getting started

A simple microservice for Task and Process CRUD.

FastAPI, Python 3.9+, is used for this purpose. The TDD (Test-Driven Development) approach is followed.

To get started, please follow these steps:

1. Run the Docker Compose file.
2. Open your local browser and browse to port 8070.
3. A OpenAPI documentation page will be displayed, where you can explore and experiment further.

Make sure you have Docker installed and configured before running the project.

Enjoy exploring the microservice!
